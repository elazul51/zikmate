export const userProfileMixin = {
  methods: {
    getAge: (moment, user) => {
      if (!user.birthdate) {
        return null;
      }
      const age = moment().diff(user.birthdate, 'years');
      return age > 1 ? `${age} ans` : `${age} an`;
    },
    getGender: user => {
      if (user.gender === 'male') {
        return '<i title="Homme" class="fa fa-mars"></i>';
      } else if (user.gender === 'female') {
        return '<i title="Femme" class="fa fa-venus"></i>';
      } else {
        return null;
      }
    },
    getCity: user => {
      return user.address && user.address.vicinity
        ? user.address.vicinity
        : null;
    },
    getInstruments: (instruments, user) => {
      if (!instruments && !user && !user.instruments) {
        return null;
      }
      return instruments
        .filter(
          instrument =>
            user.instruments ? user.instruments.includes(instrument.id) : null
        )
        .map(instrument => (instrument ? instrument.name : null))
        .join(', ');
    }
  }
};
