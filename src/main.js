import Vue from 'vue';
import VeeValidate, { Validator } from 'vee-validate';
import fr from 'vee-validate/dist/locale/fr';
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-social/bootstrap-social.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VuejsDialog from 'vuejs-dialog';
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
import VueAutosuggest from 'vue-autosuggest';
import VueMoment from 'vue-moment';
import VueSocketIO from 'vue-socket.io';
import VueChatScroll from 'vue-chat-scroll';
import * as VueGoogleMaps from 'vue2-google-maps';
import { router } from './helpers';
import App from './App.vue';
import store from './store';

Vue.config.productionTip = false;

Vue.prototype.$http = require('axios');

Validator.localize({ fr: fr });
Vue.use(VeeValidate, {
  inject: true,
  fieldsBagName: 'veeFields',
  events: 'change',
  locale: 'fr'
});

Vue.use(BootstrapVue);
Vue.use(VuejsDialog);
Vue.use(VueAutosuggest);
Vue.use(VueMoment);
Vue.use(VueChatScroll);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyCtIEuLOGOBgPI9AUj573hkIN2p-D5O5h8',
    libraries: 'places'
  }
});

Vue.use(
  new VueSocketIO({
    debug: true,
    connection: process.env.VUE_APP_API_ROOT,
    vuex: {
      store,
      actionPrefix: 'socket',
      mutationPrefix: 'socket'
    }
  })
);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
