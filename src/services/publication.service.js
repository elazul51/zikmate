import axios from 'axios';
const isHtml = require('is-html');

import { userAccessToken } from '../helpers';

export const publicationService = {
  create,
  getAll,
  destroy
};

function create(data) {
  return axios
    .post(`${process.env.VUE_APP_API_ROOT}/publications`, {
      access_token: userAccessToken(),
      author: data.author,
      content: data.content,
      origin: data.origin
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function getAll(data) {
  return axios
    .get(`${process.env.VUE_APP_API_ROOT}/publications`, {
      params: {
        access_token: userAccessToken(),
        author: data.author,
        origin: false,
        page: data.page,
        limit: data.limit,
        sort: '-createdAt'
      }
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function destroy(publicationId) {
  return axios
    .delete(`${process.env.VUE_APP_API_ROOT}/publications/${publicationId}`, {
      params: {
        access_token: userAccessToken()
      }
    })
    .then(res => {
      if (res.status !== 204) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}
