import axios from 'axios';

import { userAccessToken } from '../helpers';

export const chatService = {
  init
};

function init() {
  return axios
    .get(`${process.env.VUE_APP_API_ROOT}/chat`, {
      params: {
        access_token: userAccessToken()
      }
    })
    .then(res => {
      if (!res.data) {
        throw new Error("Erreur lors de l'initialisation du chat");
      }
      return res.data;
    })
    .catch(error => {
      throw error;
    });
}
