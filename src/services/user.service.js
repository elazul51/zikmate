import axios from 'axios';
const isHtml = require('is-html');

import {
  userAccessToken,
  authHeaderApiKey,
  authHeaderPassword
} from '../helpers';

export const userService = {
  login,
  logout,
  register,
  authWithFB,
  authWithGoogle,
  passwordReset,
  passwordFormDisplay,
  passwordUpdate,
  verify,
  update,
  updatePassword,
  get,
  getAll,
  destroy,
  sendPrivateMessage
};

function login(data) {
  return axios
    .post(
      `${process.env.VUE_APP_API_ROOT}/auth`,
      { access_token: process.env.VUE_APP_API_MASTER_KEY },
      { headers: authHeaderPassword(data) }
    )
    .then(res => {
      if (!res.data) {
        throw new Error();
      }
      localStorage.setItem('token', JSON.stringify(res.data.token));
      localStorage.setItem('user', JSON.stringify(res.data.user));
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else if (
        error.response.status === 401 &&
        error.response.statusText === 'Unauthorized'
      ) {
        throw new Error(
          'Vos identifiants de connexion sont invalides. Veuillez recommencer.'
        );
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function logout() {
  localStorage.removeItem('token');
  localStorage.removeItem('user');
}

function register(data) {
  return axios
    .post(`${process.env.VUE_APP_API_ROOT}/users`, data, {
      headers: authHeaderApiKey()
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function authWithFB() {
  // prettier-ignore
  return new Promise((resolve, reject) => {
    return FB.login(response => { // eslint-disable-line no-undef
        if (response.status === 'connected' && response.authResponse) {
          resolve(response.authResponse.accessToken);
        } else {
          reject(new Error('La connexion à Facebook a échoué'));
        }
      },
      { scope: 'email' }
    )
  })
  .then(accessToken => {
    return axios.post(`${process.env.VUE_APP_API_ROOT}/auth/facebook`, {
      access_token: accessToken
    });
  })
  .then(res => {
    if (!res.data) {
      throw new Error();
    }
    localStorage.setItem('token', JSON.stringify(res.data.token));
    localStorage.setItem('user', JSON.stringify(res.data.user));
    return res.data;
  })
  .catch(err => {
    if (err.message) {
      throw err;
    } else {
      throw new Error('Une erreur inconnue est survenue');
    }
  });
}

function authWithGoogle() {
  return gapi.auth2 // eslint-disable-line no-undef
    .getAuthInstance()
    .signIn()
    .then(() => {
      return gapi.auth2.getAuthInstance().currentUser.get(); // eslint-disable-line no-undef
    })
    .then(user => {
      return axios.post(`${process.env.VUE_APP_API_ROOT}/auth/google`, {
        access_token: user.getAuthResponse().access_token
      });
    })
    .then(res => {
      if (!res.data) {
        throw new Error();
      }
      localStorage.setItem('token', JSON.stringify(res.data.token));
      localStorage.setItem('user', JSON.stringify(res.data.user));
      return res.data;
    })
    .catch(err => {
      if (err.message) {
        throw err;
      } else {
        throw new Error('La connexion à Google a échoué');
      }
    });
}

function passwordReset(email) {
  return axios
    .post(
      `${process.env.VUE_APP_API_ROOT}/password-resets`,
      { email },
      { headers: authHeaderApiKey() }
    )
    .then(res => {
      if (res.status !== 202) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else if (error.response && error.response.status === 404) {
        throw new Error(
          "L'adresse email indiquée n'existe pas dans nos données."
        );
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function passwordFormDisplay(token) {
  return axios
    .get(`${process.env.VUE_APP_API_ROOT}/password-resets/${token}`)
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else if (error.response && error.response.status === 404) {
        throw new Error(
          'Le lien de réinitialisation du mot de passe est invalide ou a expiré. Veuillez faire une nouvelle demande.'
        );
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function passwordUpdate(data) {
  return axios
    .put(`${process.env.VUE_APP_API_ROOT}/password-resets/${data.token}`, {
      password: data.password
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function verify(token) {
  return axios
    .post(
      `${process.env.VUE_APP_API_ROOT}/users/verify`,
      { token },
      { headers: authHeaderApiKey() }
    )
    .then(res => {
      if (res.status !== 202) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function updatePassword(user) {
  return axios
    .put(`${process.env.VUE_APP_API_ROOT}/users/${user.id}/password`, {
      access_token: userAccessToken(),
      password: user.password
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function update(data, selfUpdate) {
  return axios
    .put(`${process.env.VUE_APP_API_ROOT}/users/${data.id}`, {
      access_token: userAccessToken(),
      newPicture: data.newPicture,
      firstname: data.firstname,
      lastname: data.lastname,
      gender: data.gender,
      birthdate: data.birthdate,
      phone: data.phone,
      address: data.address,
      instruments: data.instruments,
      presentation: data.presentation
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      if (selfUpdate) {
        localStorage.setItem('user', JSON.stringify(res.data));
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function get(userId) {
  const id = userId ? userId : 'me';
  return axios
    .get(`${process.env.VUE_APP_API_ROOT}/users/${id}`, {
      params: {
        access_token: userAccessToken()
      }
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function getAll(data) {
  return axios
    .get(`${process.env.VUE_APP_API_ROOT}/users`, {
      params: {
        access_token: userAccessToken(),
        _id: data && data.userId,
        q: data && data.search ? data.search : undefined,
        page: data && data.page ? data.page : undefined,
        limit: data && data.limit ? data.limit : undefined,
        sort: data && data.sort ? data.sort : 'username'
      }
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function destroy(userId) {
  return axios
    .delete(`${process.env.VUE_APP_API_ROOT}/users/${userId}`, {
      params: {
        access_token: userAccessToken()
      }
    })
    .then(res => {
      if (res.status !== 204) {
        throw new Error();
      }
      return null;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function sendPrivateMessage(data) {
  return axios
    .post(`${process.env.VUE_APP_API_ROOT}/users/send-message`, {
      access_token: userAccessToken(),
      message: data.message,
      fromUser: data.fromUser,
      toUser: data.toUser
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}
