import axios from 'axios';

import { userAccessToken } from '../helpers';

export const instrumentService = {
  getAll
};

function getAll() {
  return axios
    .get(`${process.env.VUE_APP_API_ROOT}/instruments`, {
      params: {
        access_token: userAccessToken(),
        sort: 'createdAt'
      }
    })
    .then(res => {
      if (!res.data) {
        throw new Error('Erreur lors de la récupération des instruments');
      }
      return res.data;
    })
    .catch(error => {
      throw error;
    });
}
