import axios from 'axios';
const isHtml = require('is-html');

import { userAccessToken } from '../helpers';

export const friendshipService = {
  create,
  getAll,
  destroy,
  update
};

function create(data) {
  return axios
    .post(`${process.env.VUE_APP_API_ROOT}/friendships`, {
      access_token: userAccessToken(),
      fromUser: data.fromUser,
      toUser: data.toUser
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function getAll(data) {
  return axios
    .get(`${process.env.VUE_APP_API_ROOT}/friendships`, {
      params: {
        access_token: userAccessToken(),
        fromUser: data.userId,
        toUser: data.userId,
        accepted: data.accepted,
        page: data.page,
        limit: data.limit,
        sort: '-createdAt'
      }
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function destroy(friendshipId) {
  return axios
    .delete(`${process.env.VUE_APP_API_ROOT}/friendships/${friendshipId}`, {
      params: {
        access_token: userAccessToken()
      }
    })
    .then(res => {
      if (res.status !== 204) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}

function update(friendshipId) {
  return axios
    .put(`${process.env.VUE_APP_API_ROOT}/friendships/${friendshipId}`, {
      access_token: userAccessToken(),
      accepted: true
    })
    .then(res => {
      if (res.status !== 200) {
        throw new Error();
      }
      return res.data;
    })
    .catch(error => {
      if (
        error.response &&
        error.response.data &&
        error.response.data.message &&
        !isHtml(error.response.data.message)
      ) {
        throw new Error(error.response.data.message);
      } else {
        throw new Error('Une erreur inconnue est survenue');
      }
    });
}
