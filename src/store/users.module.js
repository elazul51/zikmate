import { userService } from '../services';

const state = {
  all: {},
  current: {
    user: {}
  }
};

const actions = {
  getAll({ dispatch, commit }, data) {
    commit('getAllRequest');

    userService.getAll(data).then(
      data => {
        commit('getAllSuccess', data);
      },
      error => {
        commit('getAllFailure', error);
        const alert = {
          title: 'Erreur lors de la récupération des utilisateurs',
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  },
  get({ dispatch, commit, rootState }, userId) {
    commit('getRequest');

    userId = userId === rootState.account.user.id ? 'me' : userId;
    userService.get(userId).then(
      data => {
        commit('getSuccess', data);
      },
      error => {
        commit('getFailure', error);
        const alert = {
          title: "Erreur lors de la récupération de l'utilisateur",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  },
  sendPrivateMessage({ dispatch, commit }, data) {
    commit('sendPrivateMessageRequest');

    userService.sendPrivateMessage(data).then(
      data => {
        commit('sendPrivateMessageSuccess', data);
        const alert = {
          title: 'Envoi de message privé',
          message:
            '<p>Votre message privé a été envoyé avec succès à son destinataire.</p>'
        };
        dispatch('alert/success', alert, { root: true });
      },
      error => {
        commit('sendPrivateMessageFailure', error);
        const alert = {
          title: "Erreur lors de l'envoi du message privé'",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  }
};

const mutations = {
  getAllRequest(state) {
    state.all = { loading: true };
  },
  getAllSuccess(state, users) {
    state.all = { loaded: true, users };
  },
  getAllFailure(state, error) {
    state.all = { error };
  },
  getRequest(state) {
    state.current = { loading: true, user: {} };
  },
  getSuccess(state, user) {
    state.current = { user };
  },
  getFailure(state, error) {
    state.current = { error, user: {} };
  },
  sendPrivateMessageRequest(state) {
    state.current = { sendingMessage: true };
  },
  sendPrivateMessageSuccess(state) {
    state.current = { sentMessage: true };
  },
  sendPrivateMessageFailure(state, error) {
    state.current = { error };
  }
};

export const users = {
  namespaced: true,
  state,
  actions,
  mutations
};
