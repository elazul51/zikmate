const state = {
  show: false,
  type: null,
  title: null,
  message: null
};

const actions = {
  success({ commit }, data) {
    commit('success', data);
  },
  error({ commit }, data) {
    commit('error', data);
  },
  clear({ commit }, data) {
    commit('clear', data);
  }
};

const mutations = {
  success(state, data) {
    state.show = true;
    state.type = 'alert-success';
    state.title = data.title;
    state.message = data.message;
  },
  warning(state, data) {
    state.show = true;
    state.type = 'alert-warning';
    state.title = data.title;
    state.message = data.message;
  },
  error(state, data) {
    state.show = true;
    state.type = 'alert-error';
    state.title = data.title;
    state.message = data.message;
  },
  clear(state) {
    state.show = false;
    state.type = null;
    state.title = null;
    state.message = null;
  }
};

export const alert = {
  namespaced: true,
  state,
  actions,
  mutations
};
