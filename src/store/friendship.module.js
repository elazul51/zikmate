import { friendshipService } from '../services';

const state = {
  status: {},
  friendships: {},
  friendshipsPending: {}
};

const actions = {
  sendFriendRequest({ dispatch, commit, rootState }, data) {
    commit('sendFriendRequestRequest');

    friendshipService.create(data).then(
      data => {
        commit('sendFriendRequestSuccess', data);
        rootState.account.user.friendships.push(data);
        rootState.users.current.user.friendships.push(data);
      },
      error => {
        commit('sendFriendRequestFailure', error);
        const alert = {
          title: "Erreur lors de l'envoi de votre demande d'ajout d'ami",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  },
  getAll({ dispatch, commit }, data) {
    commit('getAllRequest');

    friendshipService.getAll(data).then(
      data => {
        commit('getAllSuccess', data);
      },
      error => {
        commit('getAllFailure', error);
        const alert = {
          title: "Erreur lors de la récupération de la liste d'amis",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  },
  getAllPending({ dispatch, commit }, data) {
    commit('getAllPendingRequest');

    friendshipService.getAll(data).then(
      data => {
        commit('getAllPendingSuccess', data);
      },
      error => {
        commit('getAllPendingFailure', error);
        const alert = {
          title: "Erreur lors de la récupération des demandes d'amis",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  },
  destroy({ dispatch, commit, rootState }, friendshipId) {
    commit('deleteRequest');

    friendshipService.destroy(friendshipId).then(
      data => {
        commit('deleteSuccess', data);
        rootState.account.user.friendships = rootState.account.user.friendships.filter(
          friendship => friendship._id !== friendshipId
        );
      },
      error => {
        commit('deleteFailure', error);
        const alert = {
          title: 'Erreur lors de la suppression de votre relation',
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  },
  update({ dispatch, commit, rootState }, friendshipId) {
    commit('updateRequest');

    friendshipService.update(friendshipId).then(
      data => {
        commit('updateSuccess', data);
        rootState.account.user.friendships = rootState.account.user.friendships.map(
          friendship => {
            if (friendship.id === data.id) {
              return data;
            }
          }
        );
      },
      error => {
        commit('updateFailure', error);
        const alert = {
          title: "Erreur lors de l'acceptation de la relation",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  }
};

const mutations = {
  sendFriendRequestRequest(state) {
    state.status = { sending: true };
  },
  sendFriendRequestSuccess(state) {
    state.status = { sent: true };
  },
  sendFriendRequestFailure(state) {
    state.status = {};
  },
  getAllRequest(state) {
    state.friendships = {};
  },
  getAllSuccess(state, friendships) {
    state.friendships = friendships;
  },
  getAllFailure(state) {
    state.friendships = {};
  },
  getAllPendingRequest(state) {
    state.friendshipsPending = {};
  },
  getAllPendingSuccess(state, friendshipsPending) {
    state.friendshipsPending = friendshipsPending;
  },
  getAllPendingFailure(state) {
    state.friendshipsPending = {};
  },
  deleteRequest(state) {
    state.status = { deleting: true };
  },
  deleteSuccess(state) {
    state.status = { deleted: true };
  },
  deleteFailure(state) {
    state.status = {};
  },
  updateRequest(state) {
    state.status = { updating: true };
  },
  updateSuccess(state) {
    state.status = { updated: true };
  },
  updateFailure(state) {
    state.status = {};
  }
};

export const friendship = {
  namespaced: true,
  state,
  actions,
  mutations
};
