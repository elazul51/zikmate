import { userService } from '../services';
import { router } from '../helpers';

const token = JSON.parse(localStorage.getItem('token'));
const user = JSON.parse(localStorage.getItem('user'));
const state =
  token && user
    ? { status: { loggedIn: true }, user, token }
    : { status: {}, user: {}, token: null };

const actions = {
  login({ dispatch, commit }, user) {
    commit('loginRequest');

    userService.login(user).then(
      data => {
        commit('loginSuccess', data);
        router.push('/profile');
      },
      error => {
        commit('loginFailure', error);
        const alert = {
          title: 'Erreur lors de la connexion',
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  },
  logout({ commit }) {
    userService.logout();
    commit('logout');
    router.go();
  },
  register({ dispatch, commit }, user) {
    commit('registerRequest', user);

    userService
      .register(user)
      .then(user => {
        commit('registerSuccess', user);
        const alert = {
          title: 'Inscription prise en compte',
          message: `<p>Votre inscription a bien été prise en compte, vous allez recevoir un email de confirmation sur votre adresse <strong>${
            user.email
          }</strong> pour finaliser votre inscription.</p>`
        };
        dispatch('alert/success', alert, { root: true });
      })
      .catch(error => {
        commit('registerFailure', error);
        const alert = {
          title: "Erreur lors de l'inscription",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      });
  },
  authWithFB({ dispatch, commit }) {
    if (state.status.registering || state.status.loggingIn) {
      return false;
    }

    commit('thirdPartyAuthRequest');

    userService
      .authWithFB()
      .then(data => {
        commit('thirdPartyAuthSuccess', data);
        router.push('/profile');
      })
      .catch(error => {
        commit('thirdPartyAuthFailure');
        const alert = {
          title: "Erreur lors de l'authentification via Facebook",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      });
  },
  authWithGoogle({ dispatch, commit }) {
    if (state.status.registering || state.status.loggingIn) {
      return false;
    }

    commit('thirdPartyAuthRequest');

    userService
      .authWithGoogle()
      .then(data => {
        commit('thirdPartyAuthSuccess', data);
        router.push('/profile');
      })
      .catch(error => {
        commit('thirdPartyAuthFailure');
        const alert = {
          title: "Erreur lors de l'authentification via Google",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      });
  },
  passwordReset({ dispatch, commit }, email) {
    commit('passwordResetRequest');

    userService
      .passwordReset(email)
      .then(() => {
        commit('passwordResetSuccess');
        const alert = {
          title: 'Envoi du lien de réinitialisation',
          message: `<p>Le lien de réinitialisation de votre mot de passe a été envoyé sur votre adresse <strong>${email}</strong>.</p>`
        };
        dispatch('alert/success', alert, { root: true });
      })
      .catch(error => {
        commit('passwordResetFailure');
        const alert = {
          title: "Erreur d'envoi du lien de réinitialisation",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      });
  },
  passwordFormDisplay({ dispatch, commit }, tokenPasswordReset) {
    commit('passwordFormDisplayRequest');

    userService
      .passwordFormDisplay(tokenPasswordReset)
      .then(() => {
        commit('passwordFormDisplaySuccess');
        return null;
      })
      .catch(error => {
        commit('passwordFormDisplayFailure');
        return router.push('/', null, () => {
          const alert = {
            title: 'Erreur sur le lien de réinitialisation',
            message: `<p><strong>Message d'erreur:</strong><br> ${
              error.message
            }</p>`
          };
          dispatch('alert/error', alert, { root: true });
        });
      });
  },
  passwordUpdate({ dispatch, commit }, data) {
    commit('passwordUpdateRequest');

    userService
      .passwordUpdate(data)
      .then(() => {
        commit('passwordUpdateSuccess');
        return router.push('/', null, () => {
          const alert = {
            title: 'Réinitialisation du mot de passe',
            message: `<p>Votre mot de passe a été réinitialisé avec succès.</p>`
          };
          dispatch('alert/success', alert, { root: true });
        });
      })
      .catch(error => {
        commit('passwordUpdateFailure');
        const alert = {
          title: 'Erreur de réinitialisation du mot de passe',
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      });
  },
  verify({ dispatch, commit }, tokenUserVerification) {
    commit('verifyRequest');

    if (state.token || !tokenUserVerification) {
      router.push('/', null, () => {
        const alert = {
          title: 'Erreur lors de la vérification du compte utilisateur',
          message:
            "<p><strong>Message d'erreur:</strong><br>Vérification du compte impossible</p>"
        };
        dispatch('alert/error', alert, { root: true });
      });
      return Promise.resolve(null);
    }

    userService
      .verify(tokenUserVerification)
      .then(() => {
        commit('verifySuccess');
        return router.push('/', null, () => {
          const alert = {
            title: 'Bienvenue sur Zikmate !',
            message:
              'Votre inscription est terminée. Vous pouvez vous connecter sur Zikmate!'
          };
          dispatch('alert/success', alert, { root: true });
        });
      })
      .catch(error => {
        commit('verifyFailure');
        return router.push('/', null, () => {
          const alert = {
            title: 'Erreur lors de la vérification du compte utilisateur',
            message: `<p><strong>Message d'erreur:</strong><br>${
              error.message
            }</p>`
          };
          dispatch('alert/error', alert, { root: true });
        });
      });
  },
  update({ dispatch, commit, rootState }, user) {
    commit('updateRequest');

    const selfUpdate = user.id === rootState.account.user.id;

    new Promise((resolve, reject) => {
      if (user.password) {
        userService
          .updatePassword(user)
          .then(() => {
            resolve(null);
          })
          .catch(error => {
            reject(error);
          });
      }
      resolve(null);
    })
      .then(() => {
        return userService.update(user, selfUpdate);
      })
      .then(user => {
        let alert = {
          title: 'Mise à jour du profil utilisateur'
        };
        if (selfUpdate) {
          commit('updateSuccess', user);
          alert.message =
            '<p>Votre profil a bien été mis à jour avec vos informations personnelles.</p>';
        } else {
          commit('updateSuccessAdmin');
          alert.message =
            "<p>Le profil de l'utilisateur a bien été mis à jour.</p>";
        }
        dispatch('alert/success', alert, { root: true });
      })
      .catch(error => {
        commit('updateFailure');
        const alert = {
          title: 'Erreur lors de la mise à jour du profil',
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      });
  },
  destroy({ dispatch, commit, rootState }, userId) {
    commit('deleteRequest');

    userService.destroy(userId).then(
      () => {
        commit('deleteSuccess');
        if (
          rootState.users &&
          rootState.users.all &&
          rootState.users.all.users &&
          rootState.users.all.users.data
        ) {
          rootState.users.all.users.data = rootState.users.all.users.data.filter(
            user => user.id !== userId
          );
          rootState.users.all.users.totalCount--;
        } else {
          router.push('/admin');
        }
      },
      error => {
        commit('deleteFailure');
        const alert = {
          title: 'Erreur lors de la suppression du profil',
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  }
};

const mutations = {
  loginRequest(state) {
    state.status = { loggingIn: true };
  },
  loginSuccess(state, data) {
    state.status = { loggedIn: true };
    state.user = data.user;
    state.token = data.token;
  },
  loginFailure(state) {
    state.status = {};
  },
  logout(state) {
    state.status = {};
    state.user = {};
    state.token = null;
  },
  registerRequest(state) {
    state.status = { registering: true };
  },
  registerSuccess(state) {
    state.status = {};
    state.user = {};
  },
  registerFailure(state) {
    state.status = {};
  },
  thirdPartyAuthRequest(state) {
    state.status = { loggingIn: true };
  },
  thirdPartyAuthSuccess(state, data) {
    state.status = { loggedIn: true };
    state.user = data.user;
    state.token = data.token;
  },
  thirdPartyAuthFailure(state) {
    state.status = {};
    state.user = {};
    state.token = null;
  },
  passwordResetRequest(state) {
    state.status = { sendingPasswordReset: true };
  },
  passwordResetSuccess(state) {
    state.status = {};
  },
  passwordResetFailure(state) {
    state.status = {};
  },
  passwordFormDisplayRequest(state) {
    state.status = { requestingPasswordResetForm: true };
  },
  passwordFormDisplaySuccess(state) {
    state.status = {};
  },
  passwordFormDisplayFailure(state) {
    state.status = {};
  },
  passwordUpdateRequest(state) {
    state.status = { changingPassword: true };
  },
  passwordUpdateSuccess(state) {
    state.status = {};
    state.user = {};
  },
  passwordUpdateFailure(state) {
    state.status = {};
  },
  verifyRequest(state) {
    state.status = { verifyingUser: true };
  },
  verifySuccess(state) {
    state.status = {};
  },
  verifyFailure(state) {
    state.status = {};
  },
  updateRequest(state) {
    state.status = { updating: true };
  },
  updateSuccess(state, user) {
    state.status = {};
    state.user = user;
  },
  updateSuccessAdmin(state) {
    state.status = {};
  },
  updateFailure(state) {
    state.status = {};
  },
  deleteRequest(state) {
    state.status = { deleting: true };
  },
  deleteSuccess(state) {
    state.status = {};
  },
  deleteFailure(state) {
    state.status = {};
  }
};

export const account = {
  namespaced: true,
  state,
  actions,
  mutations
};
