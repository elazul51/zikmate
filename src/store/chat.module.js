import { chatService } from '../services';

const state = {
  chats: null,
  handle: '',
  status: {},
  error: null
};

const actions = {
  socketSetChat({ dispatch, commit }) {
    commit('socketSetChatRequest');

    chatService.init().then(
      data => {
        commit('socketSetChatSuccess', data);
      },
      error => {
        commit('socketSetChatFailure', error);
        const alert = {
          title: "Erreur lors de l'initialisation du chat",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  },
  socketAddChat: (context, data) => {
    context.commit('socketAddChat', data);
  },
  socketSetHandle: (context, data) => {
    context.commit('socketSetHandle', data);
  }
};

const mutations = {
  socketSetChatRequest: (state, data) => {
    state.status = { initializing: true };
    state.chats = data;
  },
  socketSetChatSuccess: (state, data) => {
    state.status = { initialized: true };
    state.chats = data;
  },
  socketSetChatFailure: (state, error) => {
    state.status = {};
    state.error = error;
  },
  socketAddChat: (state, data) => {
    state.chats.push(data);
  },
  socketSetHandle: (state, data) => {
    state.handle = data;
  }
};

export const chat = {
  namespaced: true,
  state,
  actions,
  mutations
};
