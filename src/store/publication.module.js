import { publicationService } from '../services';

const state = {
  status: {},
  publications: {}
};

const actions = {
  send({ dispatch, commit }, publication) {
    commit('sendRequest');

    publicationService.create(publication).then(
      data => {
        commit('sendSuccess', data);
      },
      error => {
        commit('sendFailure', error);
        const alert = {
          title: "Erreur lors de l'envoi de votre publication",
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  },
  getAll({ dispatch, commit }, data) {
    commit('getAllRequest');

    publicationService.getAll(data).then(
      data => {
        commit('getAllSuccess', data);
      },
      error => {
        commit('getAllFailure', error);
        const alert = {
          title: 'Erreur lors de la récupération des publications',
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  },
  destroy({ dispatch, commit }, publicationId) {
    commit('deleteRequest');

    publicationService.destroy(publicationId).then(
      data => {
        commit('deleteSuccess', data);
      },
      error => {
        commit('deleteFailure', error);
        const alert = {
          title: 'Erreur lors de la suppression de votre publication',
          message: `<p><strong>Message d'erreur:</strong><br> ${
            error.message
          }</p>`
        };
        dispatch('alert/error', alert, { root: true });
      }
    );
  }
};

const mutations = {
  sendRequest(state) {
    state.status = { publishing: true };
  },
  sendSuccess(state) {
    state.status = { published: true };
  },
  sendFailure(state) {
    state.status = {};
  },
  getAllRequest(state) {
    state.publications = {};
  },
  getAllSuccess(state, publications) {
    state.publications = publications;
  },
  getAllFailure(state) {
    state.publications = {};
  },
  deleteRequest(state) {
    state.status = { deleting: true };
  },
  deleteSuccess(state) {
    state.status = { deleted: true };
  },
  deleteFailure(state) {
    state.status = {};
  }
};

export const publication = {
  namespaced: true,
  state,
  actions,
  mutations
};
