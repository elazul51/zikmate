export function userAccessToken() {
  return JSON.parse(localStorage.getItem('token'));
}

export function authHeaderApiKey() {
  return { Authorization: `Bearer ${process.env.VUE_APP_API_MASTER_KEY}` };
}

export function authHeaderPassword(data) {
  return {
    Authorization: 'Basic ' + btoa(data.email + ':' + data.password)
  };
}
