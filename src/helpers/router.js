import Vue from 'vue';
import Router from 'vue-router';
import About from '../components/About.vue';
import PageNotFound from '../components/PageNotFound.vue';
import UserAuth from '../components/UserAuth.vue';
import PasswordReset from '../components/PasswordReset.vue';
import UserAccount from '../components/UserAccount.vue';
import UserProfile from '../components/UserProfile.vue';
import UserSearch from '../components/UserSearch.vue';
import Admin from '../components/Admin.vue';
import Friends from '../components/Friends.vue';
import Chat from '../components/Chat.vue';
import store from '../store';

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'UserProfile',
      component: UserProfile,
      meta: { requiresAuth: true }
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/admin',
      name: 'Admin',
      component: Admin,
      meta: { requiresAuth: true, adminOnly: true }
    },
    {
      path: '/search',
      name: 'UserSearch',
      component: UserSearch,
      meta: { requiresAuth: true }
    },
    {
      path: '/friends',
      name: 'Friends',
      component: Friends,
      meta: { requiresAuth: true }
    },
    {
      path: '/chat',
      name: 'Chat',
      component: Chat,
      meta: { requiresAuth: true }
    },
    {
      path: '/profile/:userId?',
      name: 'UserProfile',
      component: UserProfile,
      meta: { requiresAuth: true }
    },
    {
      path: '/settings/:userId?',
      name: 'UserAccount',
      component: UserAccount,
      meta: { requiresAuth: true }
    },
    {
      path: '/password-reset/:token',
      name: 'PasswordReset',
      component: PasswordReset,
      beforeEnter: (to, from, next) => {
        const token = to.params.token;
        store
          .dispatch('account/passwordFormDisplay', token)
          .then(() => next())
          .catch(() => next());
      }
    },
    {
      path: '/auth',
      name: 'UserAuth',
      component: UserAuth,
      beforeEnter: (to, from, next) => {
        if (!store.state.account.token) {
          next();
          return;
        }
        next('/');
      }
    },
    {
      path: '/users/verify/:token',
      name: 'UserVerify',
      beforeEnter: (to, from, next) => {
        const token = to.params.token;
        store
          .dispatch('account/verify', token)
          .then(() => next())
          .catch(() => next());
      }
    },
    {
      path: '*',
      component: PageNotFound
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.adminOnly)) {
    if (
      store.state.account.token &&
      store.state.account.user &&
      store.state.account.user.role === 'admin'
    ) {
      next();
    } else {
      next('/');
    }
  } else if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.state.account.token) {
      next();
    } else {
      next('/auth');
    }
  } else {
    next();
  }
});
